﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;

namespace MessageBrokerService
{
    class WachtrijData
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["UCCXConnectionString"]);
        LijnStats lastLijnInfo = null;

        public LijnStats GetData()
        {


            try
            {
                LijnStats lijninfo = new LijnStats();
                lijninfo.lijnen = new List<Lijn>();

                
                SqlCommand cmd = new SqlCommand("dbo.CallStats", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                if (con.State == System.Data.ConnectionState.Open) con.Close();
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                

                int ordNaam = reader.GetOrdinal("naam");
                int ordWachtrij = reader.GetOrdinal("wachtrij");
                int ordWachttijd = reader.GetOrdinal("wachttijd");
                int ordIngesprek = reader.GetOrdinal("ingesprek");
                int ordIngelogd = reader.GetOrdinal("ingelogd");
                int ordReady = reader.GetOrdinal("ready");
                int ordAvgWachttijd = reader.GetOrdinal("avgwachttijd");
                int ordCallsHandled = reader.GetOrdinal("callshandled");

                while (reader.Read())
                {
                    lijninfo.lijnen.Add(new Lijn
                    {
                        wachtrij = reader.GetInt32(ordWachtrij),
                        ingelogd = reader.GetInt32(ordIngelogd),
                        ready = reader.GetInt32(ordReady),
                        ingesprek = reader.GetInt32(ordIngesprek),
                        naam = reader.GetString(ordNaam),
                        wachttijd = reader.GetInt32(ordWachttijd),
                        avgwachttijd = reader.GetInt32(ordAvgWachttijd),
                        callshandled = reader.GetInt32(ordCallsHandled),
                    });
                }
                con.Close();

                if (lastLijnInfo == null)
                {
                    lastLijnInfo = lijninfo;

                    return lijninfo;
                }
                else
                {
                    LijnStats difference = new LijnStats();

                    List<Lijn> dl = new List<Lijn>();
                    int index = 0;

                    if (lijninfo.lijnen.Count != lastLijnInfo.lijnen.Count)
                    {
                        lastLijnInfo = lijninfo;
                        return lijninfo;
                    }

                    foreach (Lijn l in lastLijnInfo.lijnen)
                    {
                        
                        if (!l.Equals(lijninfo.lijnen[index]))
                        {
                            dl.Add(lijninfo.lijnen[index]);
                        }
                        
                        index++;
                    }
                    
                    difference.lijnen = dl;
                    lastLijnInfo = lijninfo;
                    return difference;
                }
                
            }
            catch (SqlException s)
            {

                Debug.WriteLine(s.Message);
                return new LijnStats();
            }

            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return new LijnStats();
            }
            finally
            {
                if(con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                }

            }
        }

    }

    

    public class LijnStats : IEquatable<LijnStats>
    {
        public List<Lijn> lijnen { get; set; }
        public int datetimestamp = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

        public bool Equals(LijnStats other)
        {
            if (Object.ReferenceEquals(other, null)) return false;

            if (Object.ReferenceEquals(this, other)) return true;

            return lijnen.Equals(other.lijnen);
        }
    }

    public class Lijn : IEquatable<Lijn>
    {
        public string naam { get; set; }
        public int wachtrij { get; set; }
        public int wachttijd { get; set; }
        public int avgwachttijd { get; set; }
        public int callshandled { get; set; }
        public int ingelogd { get; set; }
        public int ready { get; set; }
        public int ingesprek { get; set; }


        public bool Equals(Lijn other)
        {

            if (Object.ReferenceEquals(other, null)) return false;

            if (Object.ReferenceEquals(this, other)) return true;

            return (naam.Equals(other.naam) && wachtrij.Equals(other.wachtrij) && wachttijd.Equals(other.wachttijd) && avgwachttijd.Equals(other.avgwachttijd) && callshandled.Equals(other.callshandled) && ingelogd.Equals(other.ingelogd) && ready.Equals(other.ready) && ingesprek.Equals(other.ingesprek));
        }
    }

}
