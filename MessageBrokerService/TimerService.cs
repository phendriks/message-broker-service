﻿using System;
using System.Timers;
using System.Web.Script.Serialization;

namespace MessageBrokerService
{
    class TimerService
    {
        WachtrijData wd;
        private Timer serviceTimer;
        MqttConnection mqtc;

        public TimerService(int interval, MqttConnection mc)
        {

            serviceTimer = new Timer();
            serviceTimer.Interval = interval;
            serviceTimer.AutoReset = true;
            serviceTimer.Elapsed += ServiceTimer_Elapsed;
            this.mqtc = mc;
            wd = new WachtrijData();

        }

        public void Start()
        {
            serviceTimer.Start();
        }

        public void Stop()
        {
            serviceTimer.Stop();
        }


        private void ServiceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                LijnStats ls = wd.GetData();

                if (ls != null)
                {
                    foreach (Lijn l in ls.lijnen)
                    {
                        var jss = new JavaScriptSerializer();
                        var json = jss.Serialize(l);
                        //var json = JsonConvert.SerializeObject(l);
                        mqtc.PublishRetained("lijnen/" + l.naam, json);

                        /* // Om dit te gebruiken is het wel nodig system.reflection aan de usings toe te voegen.
                        foreach (PropertyInfo prop in l.GetType().GetProperties())
                        {
                            mqtc.PublishRetained("/lijnen/" + l.naam + "/"+ prop.Name, prop.GetValue(l).ToString());
                        }
                        */ // Dit stuk verdeeld elke property over zijn eigen subtopic.

                    }
                    mqtc.PublishRetained("last_update", ls.datetimestamp.ToString());
                }
                else
                {
                    mqtc.PublishRetained("agent/error", "Geen nieuwe data ontvangen door error. Check error logs voor meer infomatie.");
                }

                
            }
            catch (Exception ex)
            {
                mqtc.PublishRetained("agent/error", DateTime.Now.ToString() + " " +ex.Message);
            }
        }
    }
}

