﻿namespace MessageBrokerService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if (!DEBUG)
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MessageBrokerService()
            };
            ServiceBase.Run(ServicesToRun);
#else
            System.Diagnostics.Debugger.Launch();
            MessageBrokerService mbs = new MessageBrokerService();
            mbs.DebugableStart();
#endif
        }
    }
}
