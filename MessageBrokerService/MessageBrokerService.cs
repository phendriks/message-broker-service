﻿using System.Configuration;
using System.ServiceProcess;

namespace MessageBrokerService
{
    public partial class MessageBrokerService : ServiceBase
    {
        MqttConnection mqttc = new MqttConnection();
        TimerService ts;

        public MessageBrokerService()
        {
            InitializeComponent();
            ts = new TimerService(int.Parse(ConfigurationManager.AppSettings["Interval"]), mqttc);
        }

        protected override void OnStart(string[] args)
        {
            mqttc.Start();
            ts.Start();
        }

        protected override void OnStop()
        {
            ts.Stop();
            mqttc.Stop();
        }

        public void DebugableStart()
        {
            mqttc.Start();
            ts.Start();
        }

    }
}
