﻿using System;
using System.Configuration;
using System.Text;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace MessageBrokerService
{
    class MqttConnection
    {
        MqttClient mqc = new MqttClient(ConfigurationManager.AppSettings["Host"]);

        public MqttConnection()
        {
        }

        public void Start()
        {
            string ClientId = Guid.NewGuid().ToString();
            mqc.Connect(ClientId);
        }

        public void Stop()
        {
            if (mqc.IsConnected)
            {
                mqc.Disconnect();
            }
        }

        public void PublishRetained(string topic, string msg)
        {
            mqc.Publish(topic, Encoding.UTF8.GetBytes(msg), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, true); // retained messages zodat nieuwe clients de meest recente update altijd ontvangen.
        }

        public void PublishRetained(string topic, int msg)
        {
            mqc.Publish(topic, BitConverter.GetBytes(msg), MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, true); // retained messages zodat nieuwe clients de meest recente update altijd ontvangen.
        }
    }
}
